"use strict";

var Data = require('../model/Data');
var UIActions = require('../actions/UIActions');
var Part = require('../model/Part');
var Attributes = require('../model/Attributes');
var Run = require('../model/Run');
var _ = require('../lib/lodash');
var TWEEN = require('../lib/tween');

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var PlayScreen;
PlayScreen = React.createClass({

    componentDidMount: function () {
        Data.bind('gameData.monster', this.onMonsterChange);
        Data.bind('gameData.upgrades', this.onUpgradeChange);
        Data.bind('gameData.kills', this.onKillChange);
        Data.bind('ui.error', this.onError);
    },
    componentWillUnmount: function () {
        Data.unbind('gameData.monster', this.onMonsterChange);
        Data.unbind('gameData.upgrades', this.onUpgradeChange);
        Data.unbind('gameData.kills', this.onKillChange);
        Data.unbind('ui.error', this.onError);
    },
    getInitialState: function () {
        return {
            monster: Data.getObjectRef('gameData.monster'),
            upgrades: this.filterUpgrade(Data.getArray('gameData.upgrades')),
            showTips: true,
            showHistory: true,
            expeditionLog: [],
            kills:0
        };
    },

    onError: function (key, value) {
        this.setState({
            error: value
        })
    },

    onMonsterChange: function (key, value) {
        this.setState({
            monster: value
        })
    },

    onKillChange: function (key, value) {
        this.setState({
            kills: value
        })
    },

    filterUpgrade: function (allUpgrades) {
        var availableUpgrades = [];
        if (!this.state) return availableUpgrades;
        var partToUpgrade = Part.getPartById(this.state.selectedPart);
        if (partToUpgrade != null) {
            _.forOwn(allUpgrades, function (upg) {
                if (partToUpgrade.canAddPart(upg)) {
                    availableUpgrades.push(upg);
                    upg.item.simulateOn(partToUpgrade.item);
                }
            });
        }
        return _.sortByAll(availableUpgrades, ['quality', 'partType']);
    },

    onUpgradeChange: function (key, value) {
        this.setState({
            upgrades: this.filterUpgrade(value)
        })

    },

    selectedPartChange: function (e) {
        this.state.selectedPart = e.target.value; // Don't do this in real project please, it would not update de view
        this.state.selectedUpgrade = null;
        this.onUpgradeChange('', Data.getArray('gameData.upgrades'));
    },

    upgradeChange: function (e) {
        this.setState({
            selectedUpgrade: e.target.value
        });
    },

    upgradeAndSend: function () {
        var partToUpgrade = Part.getPartById(this.state.selectedPart);
        var upgrade = Part.getPartById(this.state.selectedUpgrade);
        if (partToUpgrade != null && upgrade != null && partToUpgrade.canAddPart(upgrade)) {
            partToUpgrade.addPart(upgrade);
            // notify monster update
            Data.setObjectRef('gameData.monster', this.state.monster); // TODO : move in an action

            // remove part from available  TODO : move in an action
            var upgradesArr = Data.getArray('gameData.upgrades');
            upgradesArr.splice(upgradesArr.indexOf(upgrade), 1);
            Data.setArray('gameData.upgrades', upgradesArr);

            this.send();

        } else {
            UIActions.displayError('Upgrade failed, please choose another part or another upgrade.');
        }
    },

    send: function () {
        // start the run
        this.setState({
            onRun: true,
            selectedPart: null,
            selectedUpgrade: null
        });
        this.startRun();
    },

    startRun: function () {
        this.state.run = new Run(setInterval(this.tickRun, 1000), this.state.monster);
        this.tickRun();
        this.gotoHistory();
    },

    tickRun: function () {
        if (this.state.run) {
            var result = this.state.run.tick();
            if (result) {
                if (result.charAt(result.length - 1) == '\n') {
                    result = result.substring(0, result.length - 1);
                }
                this.state.expeditionLog.unshift(result.split('\n').map(function (c, i) {
                    return <div>{(i > 0 ? '        ' : '') + c}</div>;
                }));
                this.setState({
                    expeditionLog: this.state.expeditionLog
                })
            } else {
                clearInterval(this.state.run.interval);
                this.state.expeditionLog.unshift(<div>--- End of expedition ---
                    <button className="small" onClick={this.gotoTop}>OK</button>
                </div>);
                UIActions.addUpgrades(this.state.run.loots);
                this.setState({
                    onRun: false,
                    expeditionLog: this.state.expeditionLog
                });
            }
        }
    },

    getAttributeDesc: function (attr) {
        if (attr == Attributes.speed) return "More actions for equal duration.";
        if (attr == Attributes.charism) return "Less resistance from people.";
        if (attr == Attributes.dangerousness) return "How deadly the creature is.";
        if (attr == Attributes.carefullness) return "Slower find parts but better quality.";
        return "";
    },

    toggleTips: function () {
        this.setState({
            showTips: !this.state.showTips
        });
    },

    gotoHistory: function () {
        (new TWEEN.Tween({scroll: document.body.scrollTop}))
            .to({scroll: document.body.scrollHeight}, 750)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .onUpdate(function () {
                window.scrollTo(0, this.scroll);
            }).start();
    },

    gotoTop: function () {
        (new TWEEN.Tween({scroll: document.body.scrollTop}))
            .to({scroll: 65}, 750)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .onUpdate(function () {
                window.scrollTo(0, this.scroll);
            }).start();
    },

    replaceAttrByImg: function (str) {
        return <span
            dangerouslySetInnerHTML={{__html: str.replace(/(charism|carefullness|dangerousness|speed)/gi,'<img src="images/$1-icon.png" style="vertical-align: bottom"/>')}}/>;
    },

    render: function () {

        var partsLi = [];

        this.state.monster.attributes.refresh();

        if (this.state.monster.parts.length > 0) {
            var partList = this.state.monster.parts[0].getAllParts();

            for (var partId in partList) {
                var part = partList[partId];
                var combos = [];
                var bonuses = part.item.get_bonusListArr();
                _.forOwn(bonuses, function (b) {
                    if (b && b.label && b.item == part.item) {
                        combos.push(<li key={b.label}>{this.replaceAttrByImg(b.label)}</li>);
                    }
                }, this);
                var stats = _.map(Attributes, function (attr) {
                    var value = this.item.get(attr).get_baseValue();
                    if (value > 0) value = '+' + value;

                    return (
                        <span className="attribute" key={"desc2"+attr+partId}>
                            <span className="attribute_label"><img src={'images/'+attr+'-icon.png'}/></span>
                            <span className="attribute_value">{value} </span>
                        </span>
                    );
                }, part);

                var addable = part.getAddableString();
                partsLi.push(<li key={"part"+part.id}>
                    <label htmlFor={"part"+part.id}>
                        <input type="radio" name="part" id={"part"+part.id} value={part.id}
                               onChange={this.selectedPartChange} checked={this.state.selectedPart == part.id}/>
                        {_.padLeft('>', part.level, '-') + ' ' + part.toString()} <span className="description small"> {addable.length > 0 ? '(Allows : ' + part.getAddableString() + ')' : ''}</span>

                        <div className="description">{part.description}</div>
                        <div className="centered">{stats}</div>
                        { combos.length > 0 ? <div className="description">
                            <ul>{combos}</ul>
                        </div> : null }
                    </label>
                </li>);
            }
        }

        var upgradeLi = [];
        var parent = Part.getPartById(this.state.selectedPart);
        for (var partId in this.state.upgrades) {
            var part = this.state.upgrades[partId];
            var stats = _.map(Attributes, function (attr) {
                var value = 0;
                // upgrade, show the diff
                if (parent != null && parent.partType == part.partType) {
                    value = parent.item.get(attr).get_baseValue() - this.item.get(attr).get_baseValue();
                }
                // new, just add
                else {
                    value = this.item.get(attr).get_baseValue();
                }
                if (value > 0) value = '+' + value;
                return (
                    <span className="attribute" key={"desc3"+attr+partId}>
                        <span className="attribute_label"><img src={'images/'+attr+'-icon.png'}/></span>
                        <span className="attribute_value">{value} </span>
                        </span>
                );
            }, part);
            upgradeLi.push(<li key={"upgrade"+part.id}>
                <label htmlFor={"upgrade"+part.id}><input type="radio" name="upgrade" id={"upgrade"+part.id}
                                                          value={part.id} onChange={this.upgradeChange} checked={this.state.selectedUpgrade == part.id}/>
                    {part.toString()}
                    <div className="description">{part.description}</div>
                    {parent != null && parent.partType == part.partType ? <div className="centered">Improving the {part.partType} !</div> : <div className="centered">{stats}</div>}

                </label>
            </li>);
        }

        var partColor = 'inherit';
        if (this.state.selectedPart == null) {
            partColor = 'orange';
        }

        var upgradeColor = 'inherit';
        if (this.state.selectedUpgrade == null) {
            upgradeColor = 'orange';
        }

        var description = this.state.monster.toString();

        var attributes = _.map(Attributes, function (attr) {
            var tips = null;
            if (this.state.showTips) {
                tips = <div className="description" style={{maxWidth:'150px'}}
                            key={'desc'+attr}>{this.getAttributeDesc(attr)}</div>;
            }
            return (
                <div className="attribute" key={'attribute'+attr} onClick={this.toggleTips}>
                    <span className="attribute_label"><img src={'images/'+attr+'-icon.png'}/>{attr}:&nbsp;</span>
                    <span className={' attribute_value'}>{this.state.monster.attributes.get(attr).get_value()} </span>
                    <ReactCSSTransitionGroup component="div" transitionName="scroll">
                        {tips}
                    </ReactCSSTransitionGroup>
                </div>
            );
        }, this);

        var run = null;
        if (this.state.expeditionLog.length > 0) {
            run = <div className="i-bloc run-bloc" ref="runBlock" style={{width:'100%'}}>
                <h2><span onClick={this.toggleHistory}>Expeditions log</span></h2>
                {(this.state.showHistory ?
                    <div className="historyLog">
                        {this.state.expeditionLog}
                    </div>
                    : null)
                }
            </div>;
        }

        return (
            <div className="ld33-screen ld33-play" ref="screenBloc">
                <h1 >Human's Industry</h1>

                <div className="i-bloc monster-bloc" style={{width:'100%'}}>
                    <h2>Current Monster</h2>

                    <div className="description">It
                        looks {description != '' ? 'like a ' + description : 'incomplete, I should upgrade the creature and send it look after new parts'}.
                    </div>
                    {attributes}
                </div>
                {this.state.error != '' ? <div className="error">{this.state.error}</div> : '' }
                <div className="i-bloc" style={{maxWidth:'425px', width:'100%'}}>
                    <h2>Body parts</h2>

                    <div className="description" style={{color: partColor}}>Select the part to upgrade</div>
                    <ul>
                        {partsLi}
                    </ul>

                </div>
                <div className="i-bloc" style={{maxWidth:'425px', width:'100%', marginRight:'0px'}}>
                    <h2>Upgrades Available</h2>

                    {this.state.selectedPart != null ? <div className="description" style={{color: upgradeColor}}>Select the upgrade to add</div> : null}
                    <ul>
                        {upgradeLi}
                    </ul>
                </div>
                <div style={{clear: "both", padding: '10px'}}>
                    <button className="play" disabled={this.state.selectedPart == null || this.state.selectedUpgrade == null || this.state.onRun}
                            onClick={this.upgradeAndSend}>Upgrade and send the monster
                    </button>
                    <button className="play" disabled={this.state.onRun}
                            onClick={this.send}>Just send the monster
                    </button>
                </div>
                {
                    this.state.kills < 10000 ?
                        <div className="description big">{'People remaining in the city : '+(10000 - this.state.kills)}</div>:
                        <div className="description big"><div>Finally all dead ! Those city monsters did pay for their sins !</div><div>Thanks for playing !</div></div>
                }
                {run}
            </div>
        );
    }

});

module.exports = PlayScreen;