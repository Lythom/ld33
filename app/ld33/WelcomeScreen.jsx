"use strict";

var Data = require('../model/Data');
var UIActions = require('../actions/UIActions');

var WelcomeScreen;
WelcomeScreen = React.createClass({

    componentDidMount: function () {
        //Data.bind('ui.currentScreen', this.onUIChange)
    },
    getInitialState: function () {
        return {

        };
    },

    render: function () {

        return (
            <div className="ld33-screen ld33-welcome">
                <h1>Human's Industry</h1>
                <div className="maindescription">
                    <p>Human's Industry is my secret facility. There, I recycle human bodies to create a dominating monster. The people of this heartless city will finally be usefull for something after all.</p>
                 </div>

                <button onClick={UIActions.gotoScreen.bind(null,'play')}>Play</button>
            </div>
        );
    }

});

module.exports = WelcomeScreen;