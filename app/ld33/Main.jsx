"use strict";

var Data = require('../model/Data');
var PlayScreen = require('./PlayScreen');
var WelcomeScreen = require('./WelcomeScreen');

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var Main;
Main = React.createClass({

    componentDidMount: function () {
        Data.bind('ui.currentScreen', this.onUIChange)
    },
    getInitialState: function () {
        return {
            screen: Data.getString("ui.currentScreen")
        };
    },

    onUIChange: function(key, value) {
        this.setState({
            screen: value
        });
    },

    render: function () {

        var scr = <WelcomeScreen key="welcome" />;
        switch(this.state.screen) {
            case 'play':
                scr = <PlayScreen key="play" />
                break;
        }

        return (
            <div className="ld33-inner">
                <ReactCSSTransitionGroup component="div" transitionName="screen">{scr}</ReactCSSTransitionGroup>
            </div>
        );
    }

});

module.exports = Main;