"use strict";

var Data = require('model/Data');
var MyApp = React.createFactory(require("Application"));

var Monster = require('./model/Monster');
var Part = require('./model/Part');
var RpgAttributes = require('../lib/rpgAttributes');
var Attribute = RpgAttributes.Attribute;
var AttributeSet = RpgAttributes.AttributeSet;
var Bonus = RpgAttributes.Bonus;
var AdditionBonus = RpgAttributes.AdditionBonus;
var MultiplicationBonus = RpgAttributes.MultiplicationBonus;
var Effect = RpgAttributes.Effect;
var Item = RpgAttributes.Item;
var Attributes = require('./model/Attributes');
var Parts = require('./model/Parts');
var TWEEN = require('../lib/tween');


function Init() {
    var save = localStorage.getItem('ld33-save');
    if (save != null) {
        serialijse.declarePersistable(Monster);
        serialijse.declarePersistable(Part);
        serialijse.declarePersistable(Attribute);
        serialijse.declarePersistable(AttributeSet);
        serialijse.declarePersistable(Bonus);
        serialijse.declarePersistable(AdditionBonus);
        serialijse.declarePersistable(MultiplicationBonus);
        serialijse.declarePersistable(Effect);
        serialijse.declarePersistable(Item);

        var saveObj = serialijse.deserialize(save);
        Data.setInitialState(saveObj);
    } else {

        // if no save, init new starter
        var monster = new Monster();
        // add here to collection
        var upgrades = [];
        upgrades.push(new Part(Parts.head));
        Data.setArray('gameData.upgrades', upgrades);

        var body = new Part(Parts.body);
        monster.attachPart(body);

        Data.setObjectRef('gameData.monster', monster);
    }

    window.onkeyup = function (e) {
        if (e != null) {
            e.preventDefault();
            var ctrlDown = e.ctrlKey || e.metaKey;
            if (e.keyCode === 83) { // s
                localStorage.setItem('ld33-save', serialijse.serialize(Data.getObjectRef()));
            } else if (e.keyCode === 76) { // l
                var save = localStorage.getItem('ld33-save');
                if (save != null) {
                    window.location.reload();
                }
            } else if (e.keyCode === 82) { // r
                localStorage.removeItem('ld33-save');
                window.location.reload();
            }
        }
    };

    animate();
    function animate() {
        requestAnimationFrame( animate );
        TWEEN.update();
    }

    React.render(
        MyApp(),
        document.getElementById("content")
    );
};

module.exports = Init;