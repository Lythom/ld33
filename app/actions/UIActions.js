"use strict";
var Data = require('model/Data');
var _ = require('lib/lodash');
var Attributes = require('../model/Attributes');

var lastErrorTO = null;
var UIActions = {
    gotoScreen: function (screenName) {
        Data.setString('ui.currentScreen', screenName);
    },
    displayError: function(text) {
        Data.setString('ui.error', text);
        setTimeout(function(){
            Data.setString('ui.error', '');
        }, 5000);
    },
    addUpgrades: function(lootArr) {
        var arr = Data.getArray('gameData.upgrades');
        arr = arr.concat(lootArr);
        Data.setArray('gameData.upgrades', arr);
    },
    increaseKillCount: function(killC) {
        Data.setNumber('gameData.kills', Data.getNumber('gameData.kills') + killC);
    }
}

module.exports = UIActions;