var RpgAttributes = require('../lib/rpgAttributes');
var AttributeSet = RpgAttributes.AttributeSet;
var _ = require('../lib/lodash');

function Monster() {
    this.parts = [];
    this.attributes = new AttributeSet("monster");

    //speed (a fast to collect)
    this.attributes.get("speed").set_baseValue(0);

    //carefullness (quality of collected material)
    this.attributes.get("carefullness").set_baseValue(0);

    //dangerousness (number of killed people)
    this.attributes.get("dangerousness").set_baseValue(0);

    //strength (of long the creature will resist the city before retreating)
    this.attributes.get("strength").set_baseValue(0);

    //Charism (how much resistance the city will oppose)
    this.attributes.get("charism").set_baseValue(0);

    this.kills = 0;

};

Monster.prototype.attachPart = function (part) {
    this.parts.push(part);
    this.attributes.equip(part.item)
};

Monster.prototype.toString = function () {
    var bodyPart = this.parts[0];
    if(bodyPart != null && bodyPart.parts.length > 0){
        var text = this.parts[0].getDescriptiveString();
        return text;
    }
    return "";
};

module.exports = Monster;