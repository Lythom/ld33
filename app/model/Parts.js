var Attributes = require('./Attributes');
var RpgAttributes = require('../lib/rpgAttributes');
var AdditionBonus = RpgAttributes.AdditionBonus;
var Bonus = RpgAttributes.Bonus;

/**
 * sizes :
 *  1: small
 *  2: medium
 *  3: heavy
 *  4: very large
 *  5: anything
 */
var Parts;
Parts = {
    "body": {
        partType: "body",
        description: "Slow, too big, but required.",
        adjective: 'body',
        evolutions: {
            "head": -1,
            "leg": -1,
            "arm": -1,
            "hand": -1,
            "foot": -1,
            "eye": -1,
            "nose": -1,
            "teeth": -1,
            "fleshJoint": 2
        },
        slots: [1],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.speed, -5 + Math.floor(quality)));
            bonusList.push(new AdditionBonus(Attributes.charism, -5 + Math.floor(quality)));
            bonusList.push(new AdditionBonus(Attributes.carefullness, -5 + Math.floor(quality)));
            bonusList.push(new AdditionBonus(Attributes.dangerousness, 0 + Math.floor(quality)));
            return bonusList;
        }
    },
    "head": {
        partType: "head",
        description: "…seek, …bite, …loot, …destroy, …and smile ♥.",
        adjective: 'headed',
        evolutions: {
            "eye": -1,
            "nose": -1,
            "teeth": -1,
            "hand": -1,
            "hair": 1,
            "fleshJoint": 1
        },
        slots: [1],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.speed, 1 + Math.floor(quality * 0.34)));
            bonusList.push(new Bonus(Attributes.charism, equipedOnBonusFactory('body', -6 + Math.floor(quality * 3), 1)));
            bonusList.push(new AdditionBonus(Attributes.carefullness, 1 + Math.floor(quality * 0.5)));
            bonusList.push(new AdditionBonus(Attributes.dangerousness, 0 + Math.floor(quality * 1)));
            return bonusList;
        }
    },
    "leg": {
        partType: "leg",
        description: "Help the creature do more things in a same amount of time.",
        adjective: 'walking',
        evolutions: {
            "foot": 1,
            "teeth": -1,
            "hand": 1
        },
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.speed, quality));
            bonusList.push(new AdditionBonus(Attributes.charism, -5 + quality));
            bonusList.push(new AdditionBonus(Attributes.carefullness, 0 + Math.floor(quality * 0.5)));
            bonusList.push(new AdditionBonus(Attributes.dangerousness, 0 + Math.floor(quality * 0.5)));
            return bonusList;
        }
    },
    "arm": {
        partType: "arm",
        description: "Required to manipulate a hand.",
        adjective: 'armed',
        evolutions: {
            "teeth": -1,
            "hand": 1,
            "foot": 1
        },
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.speed, 1 + Math.floor(quality * 0.34)));
            bonusList.push(new AdditionBonus(Attributes.charism, 1 + Math.floor(quality * 0.5)));
            bonusList.push(new AdditionBonus(Attributes.carefullness, -2 + Math.floor(quality * 0.5)));
            return bonusList;
        }
    },
    "foot": {
        partType: "foot",
        description: "Additionnal speed. Works great on legs.",
        adjective: 'running',
        evolutions: {
            'shoe': 1
        },
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new Bonus(Attributes.speed, equipedOnBonusFactory('leg', 2 + Math.floor(quality), quality)));
            bonusList.push(new AdditionBonus(Attributes.charism, 0 + Math.floor(quality * 0.5)));
            bonusList.push(new AdditionBonus(Attributes.carefullness, 0 + Math.floor(quality * 0.5)));
            bonusList.push(new Bonus(Attributes.dangerousness, equipedOnBonusFactory('arm', 1 + Math.floor(quality * 0.5), Math.floor(0.5 + quality * 0.5))));

            // TODO : special bonus when used on a leg
            return bonusList;
        }
    },
    "hand": {
        partType: "hand",
        description: "To preleve parts more carefully and get better quality.",
        adjective: 'handed',
        evolutions: {},
        slots: [2],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new Bonus(Attributes.speed, equipedOnBonusFactory('leg', 0 + Math.floor(quality * 0.5), Math.floor(0.5 + quality * 0.5))));
            bonusList.push(new AdditionBonus(Attributes.charism, 0 + Math.floor(quality * 0.5)));
            bonusList.push(new Bonus(Attributes.carefullness, equipedOnBonusFactory('arm', 3 + Math.floor(quality), quality)));
            bonusList.push(new AdditionBonus(Attributes.dangerousness, 1 + Math.floor(quality * 0.5)));

            // todo : special bonus when used on an arm
            return bonusList;
        }
    },
    "eye": {
        partType: "eye",
        description: "An eye for an eye…",
        adjective: 'eyed',
        evolutions: {},
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.carefullness, 2 + Math.floor(quality)));
            bonusList.push(new Bonus(Attributes.charism, equipedOnBonusFactory('head', 0, quality)));
            return bonusList;
        }
    },
    "teeth": {
        partType: "teeth",
        description: "…a tooth for a tooth",
        adjective: 'carnivore',
        evolutions: {},
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new Bonus(Attributes.dangerousness, equipedOnBonusFactory('head', 1 + quality, quality)));
            return bonusList;
        }
    },
    "hair": {
        partType: "hair",
        description: "You look so pretty !",
        adjective: 'hairy',
        evolutions: {},
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.charism, 5 + Math.floor(quality*2)));
            return bonusList;
        }
    },
    "fleshJoint": {
        partType: "fleshJoint",
        description: "It looks creepy but it's usefull to append a new body !",
        adjective: 'linked',
        evolutions: {
            "head": 1,
            "body": 1
        },
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.charism, -1 + Math.floor(quality * 0.5)));
            return bonusList;
        }
    },
    "suzyHair": {
        partType: "hair",
        quality: 11,
        description: "Used to be on Suzy's head",
        adjective: '',
        evolutions: {},
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.charism, 17));
            return bonusList;
        }
    },
    "bioMechArm": {
        partType: "arm",
        quality: 11,
        description: "A bio-mech arm I built as a student. It should work.",
        adjective: '',
        evolutions: {},
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.dangerousness, 20));
            return bonusList;
        }
    },
    "suzyShoes": {
        partType: "shoe",
        quality: 11,
        description: "One of the suzy gleaming red shoes offered by the mayor.",
        adjective: '',
        evolutions: {},
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.speed, -10));
            bonusList.push(new AdditionBonus(Attributes.charism, 20));
            return bonusList;
        }
    },
    "mickaelBody": {
        partType: "body",
        quality: 11,
        description: "All that remains from Mickael. That's too bad, there was so much to get from him !",
        adjective: '',
        evolutions: {
            "head": -1,
            "leg": -1,
            "arm": -1,
            "hand": -1,
            "foot": -1,
            "eye": -1,
            "nose": -1,
            "teeth": -1,
            "fleshJoint": 2
        },
        slots: [],
        bonusFactory: function (quality) {
            var bonusList = [];
            bonusList.push(new AdditionBonus(Attributes.speed, 10));
            bonusList.push(new AdditionBonus(Attributes.charism, 10));
            bonusList.push(new AdditionBonus(Attributes.carefullness, 10));
            bonusList.push(new AdditionBonus(Attributes.dangerousness, 10));
            return bonusList;
        }
    }
};

function equipedOnBonusFactory(parentPartType, baseValue, bonusValue) {
    return function (attributeSet, bonus) {
        if (attributeSet._name.replace(/[0-9]/g, '') === parentPartType) {
            bonus.label = "Combo : +" + bonusValue + ' ' + bonus.attributeName + ' on ' + parentPartType;
            return baseValue + bonusValue;
        } else {
            return baseValue;
        }
    }
}

module.exports = Parts;