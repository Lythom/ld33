var RpgAttributes = require('../lib/rpgAttributes');
var _ = require('../lib/lodash');
var Attributes = require('./Attributes');
var Parts = require('./Parts');
var Part = require('./Part');
var UIActions = require('../actions/UIActions');

var specials = [
    function(){
        // Suzy hairs
        var partDef = Parts.suzyHair;
        UIActions.increaseKillCount(1);
        UIActions.addUpgrades([new Part(partDef)]);
        return "Oh ! That's Suzy's hair, I'd recognize her beautifull red color anywhere !\nYou can't reject my prom invitiation this time >:D\n";
    },
    function(){
        // explosion
        UIActions.increaseKillCount(50);
        return "I gave the creature some instable tubes, it should blow at least 50 of them.\n";
    },
    function(){
        // mayor
        UIActions.increaseKillCount(1);
        return "Finally got the mayor, the worst of all, elected by… himself : he cheated the elections.\nDo you know how I know ? It's easy, this city is only filled with cheaters.\n";
    },
    function(){
        // bioMechArm
        var partDef = Parts.bioMechArm;
        UIActions.addUpgrades([new Part(partDef)]);
        return "I found in the archives the bio-mech arm I built during my studies !\nThey hide it from me after I tried to plug it on some unpleasent student. Hope it still works\n";
    },
    function(){
        // mayor+suzy
        var partDef = Parts.suzyShoes;
        UIActions.addUpgrades([new Part(partDef)]);
        return "The mayor used to date Suzy despite the 43.27 age difference.\nI believe the gifts she got from him, like those gleaming red shoes, helped her accept that difference.\n";
    },
    function(){
        // Mickael Berry
        var partDef = Parts.mickaelBody;
        UIActions.addUpgrades([new Part(partDef)]);
        return "Mickael Berry was good looking before he blew up himslef in the uncontrolled explosion he made.\n But know only the body is usable. Well, I'll take it !\n";
    }
];

/**
 * Phases :
 * 1. Moving to city
 * 2. Finding a target and loot
 * 3. Increase resistance
 * 1 tick = 4 hours
 * 1 run = max 6sec
 * @param interval
 * @param monster
 * @param onEvent
 * @constructor
 */
function Run(interval, monster, onEvent) {

    this.interval = interval;
    this.monster = monster;
    this.onEvent = onEvent;
    this.time = 0;
    this.phase = 1;
    this.resistance = 0;
    this.specials = 0;


    var spd = monster.attributes.get(Attributes.speed).get_value();
    var cha = monster.attributes.get(Attributes.charism).get_value();
    var dan = monster.attributes.get(Attributes.dangerousness).get_value();
    var car = monster.attributes.get(Attributes.carefullness).get_value();

};

Run.prototype.tick = function () {

    var result = '';
    var timeUsedDuringTick = 0;

    var travelTime = this.estimateMoveToCity();
    if (this.time >= 23.5) {
        return false;
    }
    else if (this.time + travelTime >= 23.5) {
        result = this.getMessage('gone', this.time) + result;
        this.time += travelTime;

    }
    else {

        while (timeUsedDuringTick < 4) {
            var timeTaken = 0;
            var val = Math.random();


            if (this.phase == 1) {
                // arriving (speed)
                timeTaken = this.estimateMoveToCity();
                result = this.getMessage('arrivedToCity', timeTaken) + result;
                this.phase++;
            } else if (val > 0.88) {
                result = this.specialEvent() + result;
                timeTaken += 4;
            }
            else if (this.phase == 2) {
                // targeting (carefullness, dangerousness and speed)
                timeTaken = this.estimateFindAndLoot();
                // looting (carefullness and capacity)
                var loot = this.doLoot();
                // killing (carefullness and capacity)
                var kills = this.doKill();
                UIActions.increaseKillCount(kills);
                result = this.getMessage('killed', kills) + result;
                if (loot != null) {
                    UIActions.addUpgrades([loot]);
                    result = this.getMessage('gotALoot', loot) + result;
                }
                this.phase++;
            }
            else if (this.phase == 3) {
                // resistance (charism)
                timeTaken = this.estimateResistance();
                result = this.getMessage('resisted', timeTaken) + result;
                this.phase = 2;
            }
            timeUsedDuringTick += timeTaken;
        }

        this.time += timeUsedDuringTick;
    }

    return _.padLeft(Math.floor(this.time), 2, '0') + ':' + _.padLeft(Math.floor(this.time % 1), 2, '0') + ' - ' + result;
};

Run.prototype.estimateMoveToCity = function () {
    var speed = this.monster.attributes.get(Attributes.speed).get_value();
    return 4 / (Math.max(0.5 + speed * 4 / 40, 0.5));
};

Run.prototype.estimateFindAndLoot = function () {
    var speed = this.monster.attributes.get(Attributes.speed).get_value();
    var carefullness = this.monster.attributes.get(Attributes.carefullness).get_value();
    var charism = this.monster.attributes.get(Attributes.charism).get_value();
    return Math.min(4, 2 / (Math.max(0.5 + speed / 10, 0.5)) + carefullness * 0.1 - charism * 0.1);
};

Run.prototype.estimateResistance = function () {
    var dangerousness = this.monster.attributes.get(Attributes.dangerousness).get_value();
    var charism = this.monster.attributes.get(Attributes.charism).get_value();
    return Math.min(12, dangerousness / 2 - charism / 10);
};

Run.prototype.doLoot = function () {
    var partDef = _.sample(_.filter(_.values(Parts),
        function(p){return p.quality == null || p.quality < 11;}
    ));
    var carefullness = this.monster.attributes.get(Attributes.carefullness).get_value();
    var quality = 1 + Math.round(Math.random()*carefullness/10);
    partDef.quality = quality;
    return new Part(partDef);
};

Run.prototype.doKill = function () {
    var dangerousness = this.monster.attributes.get(Attributes.dangerousness).get_value();
    return Math.max(0, Math.round(Math.random()*dangerousness*5));
};

Run.prototype.specialEvent = function () {
    var specialFn = _.sample(specials);
    if (specialFn != null) {
        specials.splice(specials.indexOf(specialFn), 1); // An event occured only once
        return specialFn.call(this);
    }
    return '';
};

Run.prototype.getMessage = function (category, param) {
    switch(category){
        case 'arrivedToCity':
            return 'Departing… The creature finally arrived in town after a '+Math.round(param*100)/100+' hours… walk ?\nCould be faster with some speed.\n';
        case 'killed':
            return 'Got some people down, yay ! '+param+' less of those haters ! This creature is a real danger !\n';
        case 'gotALoot':
            return 'Found a '+param.toString()+'.\nNot bad, but could be better by retrieving it more carefully !\n';
        case 'resisted':
            return 'The people are trying to kick the creature\'s ass out.\nIt has none, but it took '+Math.round(param*100)/100+' hours to escape them. More charism would help to charm them.\n';
        case 'gone':
            return 'Finally heading home after a '+Math.round(param*100)/100+' hours long expedition.\n';
    }
    return category;
};

module.exports = Run;