function getQualityDescription (quality) {
    if (quality == 1)
        return "barely viable";
    if (quality == 2)
        return "terrible";
    if (quality == 3)
        return "shredded";
    if (quality == 4)
        return "damaged";
    if (quality == 5)
        return "patched";
    if (quality == 6)
        return "good looking";
    if (quality == 7)
        return "pretty";
    if (quality == 8)
        return "awesome";
    if (quality == 9)
        return "epic";
    if (quality == 10)
        return "legendary";

    return "";
}

module.exports = {
    getQualityDescription: getQualityDescription
};