var RpgAttributes = require('../lib/rpgAttributes');
var Item = RpgAttributes.Item;
var _ = require('../lib/lodash');
var Parts = require('./Parts');

var id = 0;
var parts = {};

function Part(opts) {
    this.id = id++;
    this.partType = opts.partType;
    this.quality = opts.quality || 1;
    this.description = opts.description;
    this.adjective = opts.adjective;
    this.evolutions = opts.evolutions; // key: partName, value: number of possible evolutions remaining (-1 for infinite)
    this.slots = opts.slots; // array of size. can hold one loot of indicated size per entry.
    this.parts = [];
    this.item = new Item(this.partType + this.id);
    this.item.partType == this.partType;
    this.level = 1;
    this.bonusFactory = opts.bonusFactory;
    var bonusList = opts.bonusFactory(this.quality);
    _.forOwn(bonusList, function (b) {
        this.item.add(b);
    }.bind(this));

    parts[this.id] = this;
};

Part.getPartById = function (id) {
    return parts[id];
};

Part.prototype.canAddPart = function (part) {
    if (!part) return false;
    if (this.partType == part.partType && this.quality < 10 && part.quality >= this.quality) return true;
    var partCounts = this.getDirectPartCounts();
    var currentlyEquipped = (partCounts[part.partType] ? partCounts[part.partType] : 0);
    return part.partType != null && this.evolutions[part.partType] !== undefined && this.evolutions[part.partType] - currentlyEquipped != 0;
};

Part.prototype.addPart = function (part) {
    if (!part) return false;
    if (part.partType == this.partType && part.quality >= this.quality) {
        if (part.quality == this.quality) {
            this.quality++;
        } else {
            this.quality = part.quality;
        }
        var bonusToRemove = this.item.get_bonusListArr().slice();
        _.forOwn(bonusToRemove, function (b) {
            this.item.remove(b);
        }.bind(this));

        var bonusToAdd = this.bonusFactory(this.quality);
        _.forOwn(bonusToAdd, function (b) {
            this.item.add(b);
        }.bind(this));

        this.item.refresh();

    }
    else if (part instanceof Part && this.canAddPart(part)) {
        this.item.equip(part.item);
        this.parts.push(part);
    }
};

Part.prototype.getAllParts = function (level) {
    if (!level) level = 1;
    var parts = [this];
    this.level = level;
    _.forOwn(this.parts, function (p) {
        parts = parts.concat(p.getAllParts(level + 1));
    });
    return parts;
};

Part.prototype.getPartCounts = function () {
    var parts = this.getAllParts();
    parts.splice(0, 1);
    var counts = {};
    _.forOwn(parts, function (p) {
        if (!counts[p.adjective]) {
            counts[p.adjective] = 1;
        } else {
            counts[p.adjective]++;
        }
    });
    return counts;
};

Part.prototype.getDirectPartCounts = function () {
    var parts = this.parts.slice();
    var counts = {};
    _.forOwn(parts, function (p) {
        if (!counts[p.partType]) {
            counts[p.partType] = 1;
        } else {
            counts[p.partType]++;
        }
    });
    return counts;
};

Part.prototype.getMeanQuality = function () {
    var parts = this.getAllParts();
    if (parts.length == 0) return 1;
    var quality = 0;
    _.forOwn(parts, function (p) {
        quality += p.quality;
    });
    return Math.round(quality / parts.length);
};

Part.prototype.getQualityDescription = function (quality) {
    if (quality == 1)
        return "barely viable";
    if (quality == 2)
        return "terrible";
    if (quality == 3)
        return "shredded";
    if (quality == 4)
        return "damaged";
    if (quality == 5)
        return "patched";
    if (quality == 6)
        return "good looking";
    if (quality == 7)
        return "pretty";
    if (quality == 8)
        return "awesome";
    if (quality == 9)
        return "epic";
    if (quality == 10)
        return "legendary";
    if (quality == 11)
        return "unique";

    return "";
};

Part.prototype.toString = function () {
    return this.getQualityDescription(this.quality) + " " + this.partType;
};

Part.prototype.getDescriptiveString = function () {
    var text = '';
    var counts = this.getPartCounts();
    for (label in counts) {
        if (text !== '') text += ', ';
        var value = counts[label];
        if (value == 1) text += 'mono-';
        if (value == 2) text += '';
        if (value == 3) text += 'tri-';
        if (value == 4) text += 'quadra-';
        if (value == 4) text += 'massively-';
        text += label;
    }
    var quality = this.getQualityDescription(this.getMeanQuality());
    return text + " " + quality + " monster";
};

Part.prototype.getAddableString = function () {
    var partEvols = (Parts[this.partType] != null ? Parts[this.partType].evolutions : null);
    var text = '';
    if (!partEvols) return '';
    var partCounts = this.getDirectPartCounts();
    _.forOwn(partEvols, function (partMax, partKey) {
        var currentlyEquipped = (partCounts[partKey] ? partCounts[partKey] : 0);
        var remaining = partEvols[partKey] - currentlyEquipped;
        if (remaining != 0) {
            if (text != '') {
                text += ', ';
            }
            text += partKey;
            if (remaining > 0) {
                text += ' x' + remaining;
            }
        }
    });
    return text;
};

module.exports = Part;